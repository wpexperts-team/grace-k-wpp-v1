<?php

namespace Grace_K_WPP;

class Main_Admin
{

    public function __construct()
    {
        add_action('admin_enqueue_scripts', array( $this, 'enqueue_styles' ));
        add_action('admin_enqueue_scripts', array( $this, 'enqueue_scripts' ));
    }

    public function enqueue_styles()
    {
		wp_enqueue_style('plugin-name-admin-styles', GRACE_K_WPP_URL . '/admin/assets/css/main.css');
		wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
    }

    public function enqueue_scripts()
    {
        // wp_enqueue_script( 'slick-slider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), null, true );
        // wp_enqueue_script('slick-slider-settings', GRACE_K_WPP_URL . '/blocks/grace-k-image-slider/slider-settings.js');
    }
}
