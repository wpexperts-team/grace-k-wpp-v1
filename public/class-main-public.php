<?php

namespace Grace_K_WPP;

class Main_Public
{

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	public function enqueue_styles() {
		wp_enqueue_style( 'plugin-name-public-styles', GRACE_K_WPP_URL . '/public/assets/css/main.css' );
		wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
		// wp_enqueue_style( 'slick-theme', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css' );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'slick-slider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), null, true );
		// wp_enqueue_script( 'slick-slider-settings', GRACE_K_WPP_URL . '/blocks/grace-k-image-slider/slider-settings.js' );
	}
}
