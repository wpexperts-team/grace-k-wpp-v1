const path = require("path");
const webpack = require("webpack");
const miniCSSExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    "./assets/js/editor.blocks": "./blocks/index.js",
    "./assets/css/blocks-style": "./src/sass/blocks-style.scss",
    "./assets/css/blocks-editor-style": "./src/sass/blocks-editor-style.scss",
  },
  output: {
    path: path.resolve(__dirname),
    // filename: "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.s?css$/,
        exclude: ["/node_modules"],
        use: [
          "style-loader",
          {
            loader: miniCSSExtractPlugin.loader,
          },
          {
            loader: "css-loader",
            options: {
              url: false,
              sourceMap: false,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [require("autoprefixer")],
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: false,
            },
          },
        ],
      },
    ],
  },
  plugins: [new miniCSSExtractPlugin({})],
};
