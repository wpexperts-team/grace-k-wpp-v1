<?php
/**
 * Plugin Name:       Grace K
 * Plugin URI:
 * Description:       Grace K
 * Version:           1.0.0
 * Author:            WP Experts
 * Author URI:        https://wpexperts.be
 * Requires at least: 5.3
 * Tested up to:      5.3
 * Text Domain:       grace-k
 * Domain Path:       /languages
 */

 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'GRACE_K_WPP_URL', plugin_dir_url( __DIR__ ) . basename( dirname( __FILE__ ) ) );
define( 'GRACE_K_WPP_PATH', plugin_dir_path( __DIR__ ) . basename( dirname( __FILE__ ) ) );

if ( ! class_exists( 'Grace_K_WPP' ) ) {

	class Grace_K_WPP {
		protected static $instance = null;

		public function __construct() {
			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
			$this->admin();
			$this->public();
			add_action('enqueue_block_assets', [$this, 'enqueue_block_assets']);
			add_action('enqueue_block_editor_assets', [$this, 'enqueue_block_editor_assets']);
		}

		public function load_plugin_textdomain() {
			load_plugin_textdomain( 'grace-k', false, basename( dirname( __FILE__ ) ) . '/languages' );
		}

		public function admin() {
			require_once 'admin/class-main-admin.php';
			$admin = new \Grace_K_WPP\Main_Admin();
			// Post Type Story
			require_once 'includes/class-story.php';
			$stories = new \Grace_K_WPP\Story();
			// Post Type Kind Word
			require_once 'includes/class-kind-word.php';
			$kind_words = new \Grace_K_WPP\Kind_Word();

			// Blocks
			require_once 'blocks/grace-k-block-max-width/index.php';
			require_once 'blocks/grace-k-stories-overview/index.php';
			require_once 'blocks/grace-k-kind-words-overview/index.php';
			require_once 'blocks/grace-k-two-images/index.php';
			require_once 'blocks/grace-k-three-images/index.php';
			require_once 'blocks/grace-k-image-content/index.php';
			require_once 'blocks/grace-k-cover-image/index.php';
			require_once 'blocks/grace-k-titles/index.php';
			require_once 'blocks/grace-k-block-style-1/index.php';
			require_once 'blocks/grace-k-image-slider/index.php';
		}

		public function public() {
			require_once 'public/class-main-public.php';
			$public = new \Grace_K_WPP\Main_Public();
		}

		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		public function enqueue_block_assets() {
			wp_enqueue_script(
				'grace-k-wpp-blocks-js',
				GRACE_K_WPP_URL . '/assets/js/editor.blocks.js',
				[], filemtime(GRACE_K_WPP_PATH . '/assets/js/editor.blocks.js')
			);
			wp_enqueue_style(
				'grace-k-wpp-blocks-style',
				GRACE_K_WPP_URL . '/assets/css/blocks-style.css',
				[], filemtime(GRACE_K_WPP_PATH . '/assets/css/blocks-style.css')
			);
		}

		public function enqueue_block_editor_assets() {
			wp_enqueue_style(
				'grace-k-wpp-blocks-editor-style',
				GRACE_K_WPP_URL . '/assets/css/blocks-editor-style.css',
				array(), ''
			);
		}
	}
}

function grace_K_WPP() {
	return Grace_K_WPP::instance();
}
grace_K_WPP();


function flush_rewrites() {
	require_once 'includes/class-story.php';
	$stories = new \Grace_K_WPP\Story();
	$stories->register_post_type();
	flush_rewrite_rules();
}

register_deactivation_hook( __FILE__, 'flush_rewrites' );
register_activation_hook( __FILE__, 'flush_rewrites' );
