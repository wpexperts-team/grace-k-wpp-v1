import "./style.scss";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InspectorControls, MediaUpload } = wp.blockEditor;
const { Fragment } = wp.element;
const {
  ServerSideRender,
  Panel,
  PanelBody,
  PanelRow,
  Button,
  TextControl,
  SelectControl
} = wp.components;

export default registerBlockType("grace-k/block-style-1", {
  title: __("Block Style 1", "grace-k"),
  description: __("Block Style 1", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Block Style 1", "grace-k")],
  attributes: {
    letter: {
      type: "string"
    },
    bgImgID: {
      type: "number"
    },
    bgImgURL: {
      type: "string"
    },
    bgImgSize: {
      type: "string"
    }
  },
  supports: {},
  edit: props => {
    const { className, setAttributes, attributes } = props;
    const { letter, bgImgID, bgImgURL, bgImgSize } = props.attributes;

    const onSelectImage = image => {
      console.log(image);
      setAttributes({
        bgImgID: image.id,
        bgImgURL: image.sizes.large.url
      });
    };
    const onRemoveImage = () => {
      setAttributes({
        bgImgID: null,
        bgImgURL: null
      });
    };

    const sizes = () => {
      console.log(props.attributes);
      return [
        { label: "Medium", bgImgSize: "50%" },
        { label: "Small", bgImgSize: "25%" }
      ];
    };

    return (
      <div>
        <Fragment>
          <InspectorControls>
            <PanelBody title={__("Settings", "grace-k")}>
              <TextControl
                label="Letter"
                value={attributes.letter}
                onChange={value => setAttributes({ letter: value })}
              />
            </PanelBody>
            <PanelBody title={__("Background Image", "grace-k")}>
              <PanelRow>
                <div>
                  <MediaUpload
                    onSelect={onSelectImage}
                    type="image"
                    value={attributes.bgImgID} // make sure you destructured backgroundImage from props.attributes!
                    render={({ open }) => [
                      <div key="test">
                        <img src={bgImgURL} alt="" className="" />
                        <button
                          type="button"
                          className="components-button is-button is-default is-large"
                          onClick={open}
                        >
                          {__("Select Image", "grace-k")}
                        </button>
                      </div>
                    ]}
                  />
                </div>
              </PanelRow>
            </PanelBody>
          </InspectorControls>
        </Fragment>
        <div className={className}>
          <div className="wp-block-grace-k-block-style-1__content">
            <div className="wp-block-grace-k-block-style-1__content-wrapper">
              <div className="wp-block-grace-k-block-style-1__letter">
                {letter}
              </div>
            </div>
          </div>
          <div className="wp-block-grace-k-block-style-1__media">
            <img
              src={bgImgURL}
              alt=""
              className="wp-block-grace-k-block-style-1__image"
            />
          </div>
        </div>
      </div>
    );
  },
  save: props => {
    const { imgID } = props.attributes;
    return null;
  }
});
