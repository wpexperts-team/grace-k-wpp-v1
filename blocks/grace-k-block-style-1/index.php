<?php

namespace Grace_K_WPP\Block\Block_Style_1;

function register_block_type_block_style_1() {

	register_block_type( 'grace-k/block-style-1', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes, $content) {
    ob_start();
    ?>
    <div class="wp-block-grace-k-block-style-1">
        <div class="wp-block-grace-k-block-style-1__content">
            <div class="wp-block-grace-k-block-style-1__content-wrapper">
                <div class="wp-block-grace-k-block-style-1__letter"><?php echo $attributes['letter'] ?></div>
            </div>
        </div>
        <div class="wp-block-grace-k-block-style-1__media">
            <?php
			$image = wp_get_attachment_image_src( $attributes['bgImgID'], 'xxl' );
			if ( $image ) {
				?>
				<img src="<?php echo $image[0]; ?>" alt="" class="wp-block-grace-k-block-style-1__image">
			<?php } ?>
        </div>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_block_style_1' );
