<?php

namespace Grace_K_WPP\Block\Block_Max_width;

function register_block_type_block_max_width()
{

    register_block_type(
        'grace-k/block-max-width', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        // 'id' => array(
        //     'type' => 'string',
        //     'source' => 'attribute',
        //     'attribute' => 'id',
        //     'selector' => '*',
        // ),
        // 'anchor' => array(
        //     'type' => 'string',
        //     'source' => 'attribute',
        //     'attribute' => 'id',
        //     'selector' => '*',
        // ),
        'render_callback' => __NAMESPACE__ . '\render_callback'
        )
    );
}

function render_callback($attributes, $content)
{
    ob_start();
    if (isset($attributes['backgroundColor'])) {
        $background_color = 'style="background-color:' . $attributes['backgroundColor'] . '"';
    }
    ?>
    <div class="wp-block-grace-k-block-max-width <?php echo $attributes['className'] . ' ' . $attributes['size'] . ' ' . $attributes['marginVerticalClass'] . ' ' . $attributes['paddingClass'] ?>" <?php echo $background_color ?>>
    <?php echo $content ?>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action('init', __NAMESPACE__ . '\register_block_type_block_max_width');
