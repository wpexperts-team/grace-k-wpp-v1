import "./style.scss";
import classNames from "classnames";

const icon = (
  <svg
    aria-hidden="true"
    focusable="false"
    data-prefix="far"
    data-icon="window-maximize"
    class="svg-inline--fa fa-window-maximize fa-w-16"
    role="img"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 512 512"
  >
    <path
      fill="currentColor"
      d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm0 394c0 3.3-2.7 6-6 6H54c-3.3 0-6-2.7-6-6V192h416v234z"
    ></path>
  </svg>
);

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks, InspectorControls } = wp.blockEditor;
const PanelColorSettings = wp.blockEditor.PanelColorSettings;
const {
  Panel,
  PanelBody,
  PanelRow,
  SelectControl,
  RangeControl,
} = wp.components;

export default registerBlockType("grace-k/block-max-width", {
  title: __("Block Max Width", "grace-k"),
  description: __(
    "The Block Max Width Block makes it easy to put some vertical dynamic flow into your page. You can us the following css class names: xs, sm, lg and xl by setting these via the 'Advanced Panel Tab' beneath.",
    "wpx"
  ),
  category: "layout",
  icon: icon,
  keywords: [__("Block Max Width", "grace-k")],
  attributes: {
    size: {
      type: "string",
    },
    backgroundColor: {
      type: "string",
    },
    backgroundColorClass: {
      type: "string",
    },
    marginVertical: {
      type: "number",
      default: 0,
    },
    marginVerticalClass: {
      type: "string",
    },
    padding: {
      type: "number",
      default: 0,
    },
    paddingClass: {
      type: "string",
    },
  },
  supports: {
    anchor: true,
  },

  edit: (props) => {
    const { className, attributes, setAttributes } = props;
    console.log(props);
    const {
      size,
      backgroundColor,
      marginVertical,
      marginVerticalClass,
      padding,
      paddingClass,
      anchor,
    } = props.attributes;
    return [
      <InspectorControls>
        <PanelBody title={__("Settings", "grace-k")}>
          <PanelRow>
            <SelectControl
              label={__("Size", "grace-k")}
              options={[
                { label: "Select", value: null },
                { label: "Extra Small", value: "xs" },
                { label: "Small", value: "sm" },
                { label: "Large", value: "lg" },
                { label: "Extra Large", value: "xl" },
              ]}
              value={size}
              onChange={(value) => {
                setAttributes({ size: value });
              }}
            />
          </PanelRow>
          <RangeControl
            label="Margin Vertical"
            min={0}
            max={8}
            value={marginVertical}
            onChange={(value) => {
              setAttributes({ marginVertical: value });
              setAttributes({
                marginVerticalClass: `u-margin-vertical-${value}`,
              });
            }}
          />
          <RangeControl
            label="Padding"
            min={0}
            max={8}
            value={padding}
            onChange={(value) => {
              setAttributes({ padding: value });
              setAttributes({
                paddingClass: `u-padding-${value}`,
              });
            }}
          />
        </PanelBody>
        <PanelColorSettings
          title={__("Background Color", "grace-k")}
          colorSettings={[
            {
              value: backgroundColor,
              onChange: (colorValue) =>
                setAttributes({ backgroundColor: colorValue }),
              label: __("Background Color"),
            },
          ]}
        />
      </InspectorControls>,
      <div
        className={classNames(
          className,
          size,
          marginVerticalClass,
          paddingClass
        )}
        style={{ backgroundColor: backgroundColor }}
      >
        <InnerBlocks />
      </div>,
    ];
  },
  save: (props) => {
    return <InnerBlocks.Content />;
  },
});
