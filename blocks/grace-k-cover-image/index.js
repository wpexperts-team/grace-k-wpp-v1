import "./style.scss";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InspectorControls, MediaUpload } = wp.blockEditor;
const { ServerSideRender, Panel, PanelBody, PanelRow, Button } = wp.components;

export default registerBlockType("grace-k/cover-image", {
  title: __("Grace K Cover Image", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Grace K Cover Image", "grace-k")],
  attributes: {
    imgID: {
      type: "number"
    },
    imgURL: {
      type: "string"
    }
  },
  supports: {},
  edit: props => {
    const { className, setAttributes, attributes } = props;
    const { imgID, imgURL } = props.attributes;
    const onSelectImage = value => {
      console.log(imgURL);
      setAttributes({
        imgID: value.id,
        imgURL: value.sizes.large.url
      });
    };
    const onRemoveImage = () => {
      setAttributes({
        imgID: null,
        imgURL: null
      });
    };
    return (
      <div className={className}>
        {!imgID ? (
          <div>
            <MediaUpload
              onSelect={onSelectImage}
              type="image"
              value={imgID}
              render={({ open }) => (
                <Button className={"button button-large"} onClick={open}>
                  {__("Upload your image", "grace-k")}
                </Button>
              )}
            ></MediaUpload>
          </div>
        ) : (
          <div className="wp-block-grace-k-cover-image">
            <img className="wp-block-grace-k-cover-image__image" src={imgURL} />
            <Button className="button button-large" onClick={onRemoveImage}>
              Remove Image
            </Button>
          </div>
        )}
      </div>
    );
  },
  save: props => {
    const { imgID } = props.attributes;
    return null;
  }
});
