<?php

namespace Grace_K_WPP\Block\Cover_Image;

function register_block_type_cover_image() {

	register_block_type( 'grace-k/cover-image', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes, $content) {
	ob_start();
    ?>
    <div class="wp-block-grace-k-cover-image">
    	<?php
		$image = wp_get_attachment_image_src( $attributes['imgID'], 'large' );
		if ( $image ) {
			?>
			<img src="<?php echo $image[0]; ?>" alt="" class="wp-block-grace-k-cover-image__image">
		<?php } ?>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_cover_image' );
