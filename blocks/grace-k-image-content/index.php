<?php

namespace Grace_K_WPP\Block\Image_Content;

include_once 'item.php';

function register_block_type_image_content() {

	register_block_type( 'grace-k/image-content', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes, $content) {
    ob_start();
    ?>
    <div class="wp-block-grace-k-image-content ">
    	<?php echo $content ?>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_image_content' );
