import "./style.scss";
import classNames from "classnames";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks, InspectorControls } = wp.blockEditor;
const { Panel, PanelBody, PanelRow, SelectControl } = wp.components;

export default registerBlockType("grace-k/image-content-item", {
  title: __("Grace K Image And Content Item", "grace-k"),
  description: __("", "grace-k"),
  parent: ["grace-k/image-content"],
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Grace K Image And Content Item", "grace-k")],
  attributes: {
    verticalAlign: {
      type: "string",
    },
    verticalAlignClass: {
      type: "string",
    },
    horizontalSpacing: {
      type: "string",
    },
    horizontalSpacingClass: {
      type: "string",
    },
  },
  supports: {},
  edit: (props) => {
    const { className, setAttributes, attributes } = props;
    const { verticalAlign, verticalAlignClass } = props.attributes;
    const { horizontalSpacing, horizontalSpacingClass } = props.attributes;
    return [
      <InspectorControls>
        <PanelBody title={__("Alignment Settings", "grace-k")}>
          <SelectControl
            label={__("Vertical Align", "grace-k")}
            options={[
              { label: "Top", value: "top" },
              { label: "Center", value: "center" },
              { label: "Bottom", value: "bottom" },
            ]}
            value={verticalAlign}
            onChange={(value) => {
              setAttributes({ verticalAlign: value });
              setAttributes({
                verticalAlignClass: `wp-block-grace-k-image-content-item--vertical-align-${value}`,
              });
            }}
          />
          <SelectControl
            label={__("Horizontal Spacing", "grace-k")}
            options={[
              { label: "None", value: null },
              { label: "Left", value: "left" },
              { label: "Right", value: "right" },
            ]}
            value={horizontalSpacing}
            onChange={(value) => {
              setAttributes({ horizontalSpacing: value });
              setAttributes({
                horizontalSpacingClass: `wp-block-grace-k-image-content-item--horizontal-spacing-${value}`,
              });
            }}
          />
        </PanelBody>
      </InspectorControls>,
      <div
        className={classNames(
          className,
          verticalAlignClass,
          horizontalSpacingClass
        )}
      >
        <InnerBlocks />
      </div>,
    ];
  },
  save: (props) => {
    return <InnerBlocks.Content />;
  },
});
