import "./style.scss";
import "./item";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks, InspectorControls, MediaUpload } = wp.blockEditor;
const { ServerSideRender, Panel, PanelBody, PanelRow, Button } = wp.components;

export default registerBlockType("grace-k/image-content", {
  title: __("Grace K Image And Content", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Grace K Image And Content", "grace-k")],
  supports: {},
  edit: props => {
    const { className, setAttributes, attributes } = props;
    return (
      <div className={className}>
        <InnerBlocks
          template={[
            ["grace-k/image-content-item", { placeholder: "" }],
            ["grace-k/image-content-item", { placeholder: "" }]
          ]}
        />
      </div>
    );
  },
  save: props => {
    return <InnerBlocks.Content />;
  }
});
