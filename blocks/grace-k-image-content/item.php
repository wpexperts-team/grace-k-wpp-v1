<?php

namespace Grace_K_WPP\Block\Image_Content_Item;

function register_block_type_image_content_item() {

	register_block_type( 'grace-k/image-content-item', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes, $content) {
    ob_start();
    ?>
    <div class="wp-block-grace-k-image-content-item <?php echo $attributes['className'] . ' ' . $attributes['verticalAlignClass'] . ' ' . $attributes['horizontalSpacingClass'] ?>">
        <div class="block-editor-inner-blocks">
            <?php echo $content ?>
        </div>

    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_image_content_item' );
