<?php

namespace Grace_K_WPP\Block\Grace_K_Titles;

function register_block_type_grace_k_title() {

	register_block_type( 'grace-k/titles', array(
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback( $attributes ) {
	ob_start();
	?>
	<div class="wp-block-grace-k-titles <?php echo $attributes['titleAlignment'] ?>">
		<div class="wp-block-grace-k-titles__container">
			<?php
			if( $attributes['subtitle'] ) { ?>
				<h4 class="wp-block-grace-k-titles__subtitle <?php echo $attributes['subtitleAlignment'] ?>"><?php echo $attributes['subtitle'] ?></h4>
			<?php }
			if( $attributes['title'] ) { ?>
				<h2 class="wp-block-grace-k-titles__title"><?php echo $attributes['title'] ?></h2>
			<?php }
			?>
		</div>
	</div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_grace_k_title' );
