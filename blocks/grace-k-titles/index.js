import "./style.scss";
import "./editor.scss";
import classNames from "classnames";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const {
  Panel,
  PanelBody,
  PanelRow,
  SelectControl,
  RangeControl
} = wp.components;

export default registerBlockType("grace-k/titles", {
  title: __("Titles", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Titles", "grace-k")],
  attributes: {
    subtitle: {
      type: "string"
    },
    subtitleAlignment: {
      type: "string"
    },
    title: {
      type: "string"
    },
    titleAlignment: {
      type: "string"
    }
  },
  supports: {
    // align: true
  },
  edit: props => {
    const { className, attributes, setAttributes } = props;
    const {
      subtitle,
      subtitleAlignment,
      title,
      titleAlignment
    } = props.attributes;

    function onTitleChange(value) {
      setAttributes({
        title: value
      });
    }

    function onSubtitleChange(value) {
      setAttributes({
        subtitle: value
      });
    }

    return [
      <InspectorControls>
        <PanelBody title={__("Settings", "grace-k")}>
          <PanelRow>
            <SelectControl
              label={__("Subtitle Alignment:", "grace-k")}
              options={[
                { label: "Select", value: null },
                { label: "Left", value: "left" },
                { label: "Center", value: "center" },
                { label: "Right", value: "right" }
              ]}
              value={subtitleAlignment}
              onChange={value => {
                setAttributes({ subtitleAlignment: value });
              }}
            />
          </PanelRow>
          <PanelRow>
            <SelectControl
              label={__("Title Alignment:", "grace-k")}
              options={[
                { label: "Select", value: null },
                { label: "Left", value: "left" },
                { label: "Center", value: "center" },
                { label: "Right", value: "right" }
              ]}
              value={titleAlignment}
              onChange={value => {
                setAttributes({ titleAlignment: value });
              }}
            />
          </PanelRow>
        </PanelBody>
      </InspectorControls>,
      <div className={classNames(className, titleAlignment)}>
        <div class="wp-block-grace-k-titles">
          <div class="wp-block-grace-k-titles__container">
            <RichText
              tagName="h4"
              className={classNames(
                "wp-block-grace-k-titles__subtitle",
                subtitleAlignment
              )}
              value={subtitle}
              onChange={onSubtitleChange}
              placeholder="Enter your subtitle here"
            />
            <RichText
              tagName="h2"
              className="wp-block-grace-k-titles__title"
              value={title}
              onChange={onTitleChange}
              placeholder="Enter your title here"
            />
          </div>
        </div>
      </div>
    ];
  },
  save: props => {
    const {
      subtitle,
      subtitleAlignment,
      title,
      titleAlignment
    } = props.attributes;
    return null;
  }
});
