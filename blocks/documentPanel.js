import { registerPlugin } from "@wordpress/plugins";
// import { PluginSidebar, PluginSidebarMoreMenuItem } from "@wordpress/edit-post";
import { __ } from "@wordpress/i18n";

console.log("Loaded");

registerPlugin("myprefix-sidebar", {
  icon: "smiley",
  render: () => {
    return (
      <>
        <PluginSidebarMoreMenuItem target="myprefix-sidebar">
          {__("Meta Options", "textdomain ")}
        </PluginSidebarMoreMenuItem>
        <PluginSidebar title={__("Meta Options", "textdomain")}>
          Some Content
        </PluginSidebar>
      </>
    );
  },
});

console.log("hello test aangepast 0");
