import "./style.scss";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;
const { serverSideRender: ServerSideRender } = wp;

export default registerBlockType("grace-k/stories-overview", {
  title: __("GK Stories Overview", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Stories Overview", "grace-k")],
  supports: {},
  edit: (props) => {
    const { className } = props;
    return (
      <ServerSideRender
        block="grace-k/stories-overview"
        attributes={props.attributes}
      />
    );
  },
  save: (props) => {
    return null;
  },
});
