<?php

namespace Grace_K_WPP\Block\Grace_K_Stories_Overview;

function register_block_type_grace_k_stories_overview()
{
    register_block_type(
        'grace-k/stories-overview', array(
        'render_callback' => __NAMESPACE__ . '\render_callback'
        )
    );
}

function render_callback($attributes)
{
    ob_start();
    // Get stories
    $args    = [
        'post_type'      => 'story',
        'posts_per_page' => 6,
        'post_status'    => 'publish',
    ];
    $stories = get_posts($args);
    ?>
    <?php
    if ($stories ) {
        ?>
        <div class="wp-block-grace-k-stories-overview">
            <?php foreach ( $stories as $story ) { ?>
                <div class="c-block-3__item">
                    <a href="<?php echo get_permalink($story->ID); ?>" class="c-block-3__link">
                        <div class="c-block-4">
                            <div class="c-block-4__media">
                                <?php
                                $get_featured_image_id = get_post_thumbnail_id($story->ID);
                                $image = wp_get_attachment_image_url($get_featured_image_id, 'four_three_landscape_large');
                                ?>
                                <img src="<?php echo $image ?>" alt="" class="c-block-4__image">
                            </div>
                            <div class="c-block-4__content">
                                <h4 class="c-block-4__subtitle">
                                    <?php
                                    $client = get_field( 'story_client', $story->ID );
                                    $location =  get_field( 'location', $story->ID );
                                    echo $client;
                                    if($location) {
                                        echo ' <span class="c-block-4__location">/<span class="u-color-secondary">/</span> ' .  $location . '</span>';
                                    }

                                    ?>
                                </h4>
                                <h3 class="c-block-4__title"><?php echo $story->post_title; ?></h3>
                                <p><?php echo $story->post_excerpt; ?></p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action('init', __NAMESPACE__ . '\register_block_type_grace_k_stories_overview');
