<?php

namespace Grace_K_WPP\Block\Three_Images;

function register_block_type_three_images() {

	register_block_type( 'grace-k/three-images', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes ) {
	ob_start();
	$img1_ID = wp_get_attachment_image_src( $attributes['img1ID'], 'four_three_portrait_large' )[0];
	$img2_ID = wp_get_attachment_image_src( $attributes['img2ID'], 'four_three_landscape_large' )[0];
	$img3_ID = wp_get_attachment_image_src( $attributes['img3ID'], 'four_three_landscape_large' )[0];
	?>
	<div class="wp-block-grace-k-three-images">
		<div class="wp-block-grace-k-three-images__item-1">
			<img src="<?php echo $img1_ID?>" alt="" class="wp-block-grace-k-three-images__image wp-block-grace-k-three-images__image--portrait">
		</div>
		<div class="wp-block-grace-k-three-images__item-2">
			<img src="<?php echo $img2_ID?>" alt="" class="wp-block-grace-k-three-images__image wp-block-grace-k-three-images__image--landscape">
			<img src="<?php echo $img3_ID?>" alt="" class="wp-block-grace-k-three-images__image wp-block-grace-k-three-images__image--landscape-small">
		</div>
	</div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_three_images' );
