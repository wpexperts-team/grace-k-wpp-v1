import "./style.scss";
import "./editor.scss";

const icon = (
  <svg
    aria-hidden="true"
    focusable="false"
    data-prefix="far"
    data-icon="images"
    class="svg-inline--fa fa-images fa-w-18"
    role="img"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 576 512"
  >
    <path
      fill="currentColor"
      d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v48H54a6 6 0 0 0-6 6v244a6 6 0 0 0 6 6h372a6 6 0 0 0 6-6v-10h48zm42-336H150a6 6 0 0 0-6 6v244a6 6 0 0 0 6 6h372a6 6 0 0 0 6-6V86a6 6 0 0 0-6-6zm6-48c26.51 0 48 21.49 48 48v256c0 26.51-21.49 48-48 48H144c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h384zM264 144c0 22.091-17.909 40-40 40s-40-17.909-40-40 17.909-40 40-40 40 17.909 40 40zm-72 96l39.515-39.515c4.686-4.686 12.284-4.686 16.971 0L288 240l103.515-103.515c4.686-4.686 12.284-4.686 16.971 0L480 208v80H192v-48z"
    ></path>
  </svg>
);

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Editable, MediaUpload, InnerBlocks } = wp.blockEditor;
const { Button } = wp.components;

export default registerBlockType("grace-k/three-images", {
  title: __("3 Images", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: icon,
  keywords: [__("3 images", "grace-k")],
  attributes: {
    // First Image
    img1URL: {
      type: "string"
    },
    img1ID: {
      type: "number"
    },
    img1Alt: {
      type: "string"
    },
    // Second Image
    img2URL: {
      type: "string"
    },
    img2ID: {
      type: "number"
    },
    img2Alt: {
      type: "string"
    },
    // Third Image
    img3URL: {
      type: "string"
    },
    img3ID: {
      type: "number"
    },
    img3Alt: {
      type: "string"
    }
  },
  supports: {},
  edit: props => {
    const {
      attributes: {
        img1ID,
        img1URL,
        img1Alt,
        img2ID,
        img2URL,
        img2Alt,
        img3ID,
        img3URL,
        img3Alt
      },
      className,
      setAttributes,
      isSelected
    } = props;
    const onSelectImage1 = img1 => {
      setAttributes({
        img1ID: img1.id,
        img1URL: img1.sizes.four_three_portrait_large.url,
        img1Alt: img1.alt
      });
    };
    const onSelectImage2 = img2 => {
      setAttributes({
        img2ID: img2.id,
        img2URL: img2.sizes.four_three_landscape_large.url,
        img2Alt: img2.alt
      });
    };
    const onSelectImage3 = img3 => {
      setAttributes({
        img3ID: img3.id,
        img3URL: img3.sizes.four_three_landscape_large.url,
        img3Alt: img3.alt
      });
    };
    const onRemoveImage1 = () => {
      setAttributes({
        img1ID: null,
        img1URL: null,
        img1Alt: null
      });
    };
    const onRemoveImage2 = () => {
      setAttributes({
        img2ID: null,
        img2URL: null,
        img2Alt: null
      });
    };
    const onRemoveImage3 = () => {
      setAttributes({
        img3ID: null,
        img3URL: null,
        img3Alt: null
      });
    };
    return (
      <div className={className}>
        <div class="wp-block-grace-k-three-images__item-1">
          {!img1ID ? (
            <div>
              <MediaUpload
                onSelect={onSelectImage1}
                type="image"
                value={img1ID}
                render={({ open }) => (
                  <div>
                    <img src="https://via.placeholder.com/1200x1600?text=Image+Portrait" />
                    <br />
                    <Button className={"button button-large"} onClick={open}>
                      {__("Upload image ", "grace-k")}
                    </Button>
                  </div>
                )}
              ></MediaUpload>
            </div>
          ) : (
            <div>
              <img
                className="wp-block-grace-k-three-images__image wp-block-grace-k-three-images__image--portrait"
                src={img1URL}
                alt={img1Alt}
              />
              <Button className="button button-large" onClick={onRemoveImage1}>
                Remove Image
              </Button>
            </div>
          )}
        </div>
        <div class="wp-block-grace-k-three-images__item-2">
          {!img2ID ? (
            <div>
              <MediaUpload
                onSelect={onSelectImage2}
                type="image"
                value={img2ID}
                render={({ open }) => (
                  <div>
                    <img src="https://via.placeholder.com/1600x1200?text=Image+Landscape" />
                    <br />
                    <Button className={"button button-large"} onClick={open}>
                      {__("Upload image ", "grace-k")}
                    </Button>
                  </div>
                )}
              ></MediaUpload>
            </div>
          ) : (
            <div>
              <img
                className="wp-block-grace-k-three-images__image wp-block-grace-k-three-images__image--landscape"
                src={img2URL}
                alt={img2Alt}
              />
              <Button className="button button-large" onClick={onRemoveImage2}>
                Remove Image
              </Button>
            </div>
          )}
          {!img3ID ? (
            <div>
              <MediaUpload
                onSelect={onSelectImage3}
                type="image"
                value={img3ID}
                render={({ open }) => (
                  <div>
                    <img src="https://via.placeholder.com/1600x1200?text=Image+Landscape+Small" />
                    <br />
                    <Button className={"button button-large"} onClick={open}>
                      {__("Upload image ", "grace-k")}
                    </Button>
                  </div>
                )}
              ></MediaUpload>
            </div>
          ) : (
            <div>
              <img
                className="wp-block-grace-k-three-images__image wp-block-grace-k-three-images__image--landscape-small"
                src={img3URL}
                alt={img3Alt}
              />
              <Button className="button button-large" onClick={onRemoveImage3}>
                Remove Image
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  },
  save: props => {
    const {
      img1ID,
      img1URL,
      img1Alt,
      img2ID,
      img2URL,
      img2Alt,
      img3ID,
      img3URL,
      img3Alt
    } = props.attributes;
    return null;
  }
});
