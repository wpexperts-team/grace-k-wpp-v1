<?php

namespace Grace_K_WPP\Block\Image_Slider;

function register_block_type_image_slider()
{
    register_block_type(
        'grace-k/image-slider', array(
        'render_callback' => __NAMESPACE__ . '\render_callback'
        )
    );
}

function render_callback($attributes, $content)
{
    ob_start();
    ?>
    <div class="wp-block-grace-k-image-slider">
        <ul class="slider">
          <li class="slick-item"><img src="https://source.unsplash.com/random/1000x1000" alt=""></li>
          <li class="slick-item"><img src="https://source.unsplash.com/random/1000x1000" alt=""></li>
          <li class="slick-item"><img src="https://source.unsplash.com/random/1000x1000" alt=""></li>
        </ul>
        <script>
            jQuery(function ($) {
                $(".slider").slick({
                    dots: true,
                    speed: 500,
                    arrows: false,
                    autoplay: true,
                    infinite: true,
                    nextArrow: false,
                    prevArrow: false
                });
            });
        </script>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action('init', __NAMESPACE__ . '\register_block_type_image_slider');
