import "./style.scss";
import classNames from "classnames";

const icon = (
  <svg
    aria-hidden="true"
    focusable="false"
    data-prefix="far"
    data-icon="window-maximize"
    class="svg-inline--fa fa-window-maximize fa-w-16"
    role="img"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 512 512"
  >
    <path
      fill="currentColor"
      d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm0 394c0 3.3-2.7 6-6 6H54c-3.3 0-6-2.7-6-6V192h416v234z"
    ></path>
  </svg>
);

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks, InspectorControls } = wp.blockEditor;
const PanelColorSettings = wp.blockEditor.PanelColorSettings;
const {
  Panel,
  PanelBody,
  PanelRow,
  SelectControl,
  RangeControl,
} = wp.components;

export default registerBlockType("grace-k/image-slider", {
  title: __("Image Slider", "grace-k"),
  description: __("Grace K Image slider"),
  category: "layout",
  icon: icon,
  keywords: [__("Grace K Image Slider", "grace-k")],
  attributes: {},
  supports: {
    anchor: true,
  },

  edit: (props) => {
    const { className, attributes, setAttributes } = props;
    console.log(props);
    const {} = props.attributes;
    return [
      <InspectorControls>
        <PanelBody title={__("Settings", "grace-k")}>
          <PanelRow></PanelRow>
        </PanelBody>
      </InspectorControls>,
      <div className={classNames(className)}>
        <div class="slider single-item">
          <div>your content</div>
          <div>your content</div>
          <div>your content</div>
        </div>
        <p>En hier komt de slider</p>
      </div>,
    ];
  },
  save: (props) => {
    return <InnerBlocks.Content />;
  },
});
