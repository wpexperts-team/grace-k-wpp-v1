import "./style.scss";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;
const { serverSideRender: ServerSideRender } = wp;

export default registerBlockType("grace-k/kind-words-overview", {
  title: __("GK Kind Words Overview", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Kind Words Overview", "grace-k")],
  supports: {},
  edit: props => {
    const { className } = props;
    return (
      <ServerSideRender
        block="grace-k/kind-words-overview"
        attributes={props.attributes}
      />
    );
  },
  save: props => {
    return null;
  }
});
