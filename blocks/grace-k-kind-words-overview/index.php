<?php

namespace Grace_K_WPP\Block\Grace_K_Kind_Words_Overview;

function register_block_type_grace_k_kind_words_overview() {

	register_block_type( 'grace-k/kind-words-overview', array(
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes) {
    ob_start();
	// Get stories
    $args    = [
        'post_type'      => 'kind-word',
        'posts_per_page' => 2,
        'post_status'    => 'publish',
    ];
    $kind_words = get_posts( $args );
    ?>
    <?php
    if ( $kind_words ) {
        ?>
        <div class="wp-block-grace-k-kind-words-overview">
            <ul class="wp-block-grace-k-kind-words-overview__list">
                <?php foreach ( $kind_words as $kind_word ) {
                    $title = $kind_word->post_title;
                    $excerpt = $kind_word->post_content;
                    $image = '';
                    ?>
                    <li class="wp-block-grace-k-kind-words-overview__list-item">
                        <div class="wp-block-grace-k-kind-words-block">
                            <div class="wp-block-grace-k-kind-words-block__media">
                                <?php
                                $image = get_the_post_thumbnail( $kind_word->ID, 'four_three_landscape_large', ['class' => 'wp-block-grace-k-kind-words-block__image'] );
                                echo $image;
                                ?>
                            </div>
                            <div class="wp-block-grace-k-kind-words-block__content">
                                <h3 class="wp-block-grace-k-kind-words-block__title"><?php echo $title ?></h3>
                                <?php
                                if( isset($excerpt) ) {
                                ?>
                                    <div class="wp-block-grace-k-kind-words-block__excerpt"><?php echo $excerpt ?></div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_grace_k_kind_words_overview' );
