const { __ } = wp.i18n;
const { registerBlockStyle } = wp.blocks;

registerBlockStyle("core/image", {
  name: "trapezoid",
  label: __("Trapezoid"),
  isDefault: false
});

registerBlockStyle("core/image", {
  name: "trapezoid-reverse",
  label: __("Trapezoid Reverse"),
  isDefault: false
});

registerBlockStyle("core/image", {
  name: "trapezoid-bottom",
  label: __("Trapezoid Bottom"),
  isDefault: false
});

registerBlockStyle("core/image", {
  name: "trapezoid-bottom-reverse",
  label: __("Trapezoid Bottom Reverse"),
  isDefault: false
});

registerBlockStyle("core/heading", {
  name: "heading-line-bottom-left",
  label: __("Line Bottom Left"),
  isDefault: false
});

registerBlockStyle("core/heading", {
  name: "heading-line-bottom-center",
  label: __("Line Bottom Center"),
  isDefault: false
});

registerBlockStyle("core/heading", {
  name: "heading-line-bottom-right",
  label: __("Line Bottom Right"),
  isDefault: false
});
