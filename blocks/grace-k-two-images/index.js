import "./style.scss";
import "./editor.scss";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Editable, MediaUpload, InnerBlocks } = wp.blockEditor;
const { Button } = wp.components;

export default registerBlockType("grace-k/two-images", {
  title: __("Two Images", "grace-k"),
  description: __("", "grace-k"),
  category: "layout",
  icon: "admin-plugins",
  keywords: [__("Two images", "grace-k")],
  attributes: {
    // First Image
    img1URL: {
      type: "string"
    },
    img1ID: {
      type: "number"
    },
    img1Alt: {
      type: "string"
    },
    // Second Image
    img2URL: {
      type: "string"
    },
    img2ID: {
      type: "number"
    },
    img2Alt: {
      type: "string"
    }
  },
  supports: {},
  edit: props => {
    const {
      attributes: { img1ID, img1URL, img1Alt, img2ID, img2URL, img2Alt },
      className,
      setAttributes,
      isSelected
    } = props;
    const onSelectImage1 = img1 => {
      setAttributes({
        img1ID: img1.id,
        img1URL: img1.sizes.large.url,
        img1Alt: img1.alt
      });
    };
    const onSelectImage2 = img2 => {
      setAttributes({
        img2ID: img2.id,
        img2URL: img2.sizes.large.url,
        img2Alt: img2.alt
      });
    };
    const onRemoveImage1 = () => {
      setAttributes({
        img1ID: null,
        img1URL: null,
        img1Alt: null
      });
    };
    const onRemoveImage2 = () => {
      setAttributes({
        img2ID: null,
        img2URL: null,
        img2Alt: null
      });
    };
    return (
      <div className={className}>
        <div className="wp-block-grace-k-two-images__item-1">
          {!img1ID ? (
            <div>
              <MediaUpload
                onSelect={onSelectImage1}
                type="image"
                value={img1ID}
                render={({ open }) => (
                  <div>
                    <img
                      src="https://via.placeholder.com/1600x1200?text=Image+1"
                      alt=""
                    />
                    <br />
                    <Button className={"button button-large"} onClick={open}>
                      {__("Upload image ", "grace-k")}
                    </Button>
                  </div>
                )}
              ></MediaUpload>
            </div>
          ) : (
            <div>
              <img
                className="wp-block-grace-k-two-images__image"
                src={img1URL}
                alt={img1Alt}
              />
              <Button className="button button-large" onClick={onRemoveImage1}>
                Remove Image
              </Button>
            </div>
          )}
        </div>
        <div className="wp-block-grace-k-two-images__item-2">
          {!img2ID ? (
            <div>
              <MediaUpload
                onSelect={onSelectImage2}
                type="image"
                value={img2ID}
                render={({ open }) => (
                  <div>
                    <img src="https://via.placeholder.com/1600x1200?text=Image+2" />
                    <br />
                    <Button className={"button button-large"} onClick={open}>
                      {__("Upload image ", "grace-k")}
                    </Button>
                  </div>
                )}
              ></MediaUpload>
            </div>
          ) : (
            <div>
              <img
                className="wp-block-grace-k-two-images__image"
                src={img2URL}
                alt={img2Alt}
              />
              <Button className="button button-large" onClick={onRemoveImage2}>
                Remove Image
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  },
  save: props => {
    const {
      img1ID,
      img1URL,
      img1Alt,
      img2ID,
      img2URL,
      img2Alt
    } = props.attributes;
    return null;
  }
});
