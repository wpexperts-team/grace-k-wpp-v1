<?php

namespace Grace_K_WPP\Block\Two_Images;

function register_block_type_two_images() {

	register_block_type( 'grace-k/two-images', array(
        // 'style' => 'grace-k-wpp-blocks-styles',
        // 'script' => 'grace-k-wpp-blocks-js',
        'render_callback' => __NAMESPACE__ . '\render_callback'
    ) );
}

function render_callback($attributes ) {
	ob_start();
	$img1_ID = wp_get_attachment_image_src( $attributes['img1ID'], 'large' )[0];
	$img2_ID = wp_get_attachment_image_src( $attributes['img2ID'], 'large' )[0];
	?>
	<div class="wp-block-grace-k-two-images">
		<div class="wp-block-grace-k-two-images__item-1">
			<img src="<?php echo $img1_ID?>" alt="" class="wp-block-grace-k-two-images__image">
		</div>
		<div class="wp-block-grace-k-two-images__item-2">
			<img src="<?php echo $img2_ID?>" alt="" class="wp-block-grace-k-two-images__image">
		</div>
	</div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_action( 'init', __NAMESPACE__ . '\register_block_type_two_images' );
