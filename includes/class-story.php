<?php

namespace Grace_K_WPP;

class Story {


	public function __construct() {
		add_action( 'init', [ $this, 'register_post_type' ], 0 );
		add_action( 'init', [ $this, 'acf' ], 0 );
	}

	public function register_post_type() {
		$labels  = array(
			'name'                  => _x( 'Stories', 'Post Type General Name', 'grace-k' ),
			'singular_name'         => _x( 'Story', 'Post Type Singular Name', 'grace-k' ),
			'menu_name'             => __( 'Stories', 'grace-k' ),
			'name_admin_bar'        => __( 'Story', 'grace-k' ),
			'archives'              => __( 'Story Archives', 'grace-k' ),
			'attributes'            => __( 'Story Attributes', 'grace-k' ),
			'parent_item_colon'     => __( 'Parent Story:', 'grace-k' ),
			'all_items'             => __( 'All Stories', 'grace-k' ),
			'add_new_item'          => __( 'Add New Story', 'grace-k' ),
			'add_new'               => __( 'Add New', 'grace-k' ),
			'new_item'              => __( 'New Story', 'grace-k' ),
			'edit_item'             => __( 'Edit Story', 'grace-k' ),
			'update_item'           => __( 'Update Story', 'grace-k' ),
			'view_item'             => __( 'View Story', 'grace-k' ),
			'view_items'            => __( 'View Stories', 'grace-k' ),
			'search_items'          => __( 'Search Story', 'grace-k' ),
			'not_found'             => __( 'Not found', 'grace-k' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'grace-k' ),
			'featured_image'        => __( 'Featured Image', 'grace-k' ),
			'set_featured_image'    => __( 'Set featured image', 'grace-k' ),
			'remove_featured_image' => __( 'Remove featured image', 'grace-k' ),
			'use_featured_image'    => __( 'Use as featured image', 'grace-k' ),
			'insert_into_item'      => __( 'Insert into story', 'grace-k' ),
			'uploaded_to_this_item' => __( 'Uploaded to this story', 'grace-k' ),
			'items_list'            => __( 'Stories list', 'grace-k' ),
			'items_list_navigation' => __( 'Stories list navigation', 'grace-k' ),
			'filter_items_list'     => __( 'Filter stories list', 'grace-k' ),
		);
		$rewrite = [
			'slug' => 'story',
		];
		$args    = array(
			'label'               => __( 'Story', 'grace-k' ),
			'description'         => __( 'Stories Description', 'grace-k' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_rest'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => __( 'stories', 'grace-k' ),
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'rewrite'             => $rewrite,
			'template'            => array(
				array(
					'grace-k/block-max-width',
					array(
						'placeholder' => 'Add some inner blocks',
					),
				),
			),
		);
		register_post_type( 'story', $args );
	}

	public function acf() {
		if ( function_exists( 'acf_add_local_field_group' ) ) :
			acf_add_local_field_group(
				array(
					'key'                   => 'group_story_settings',
					'title'                 => 'Story Settings',
					'fields'                => array(
						array(
							'key'               => 'story_client',
							'label'             => 'Client',
							'name'              => 'story_client',
							'type'              => 'text',
							'instructions'      => __( '', 'grace-k' ),
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'location',
							'label'             => 'Location',
							'name'              => 'location',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'story_teaser_video_vimeo_id',
							'label'             => 'Teaser Video Vimeo ID',
							'name'              => 'story_teaser_video_vimeo_id',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'story_gallery',
							'label'             => 'gallery',
							'name'              => 'story_gallery',
							'type'              => 'gallery',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'thumbnail',
							'insert'            => 'append',
							'library'           => 'all',
							'min'               => '',
							'max'               => '',
							'min_width'         => '',
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => 'png,jpg',
						),
						array(
							'key'               => 'story_after_movie_vimeo_id',
							'label'             => 'After Movie Vimeo ID',
							'name'              => 'story_after_movie_vimeo_id',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post_type',
								'operator' => '==',
								'value'    => 'story',
							),
						),
					),
					'menu_order'            => 0,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
				)
			);
		endif;
	}
}
