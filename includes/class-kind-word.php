<?php

namespace Grace_K_WPP;

class Kind_Word {

	public function __construct() {
		add_action( 'init', [ $this, 'register_post_type' ], 0 );
	}

	public function register_post_type() {
		$labels  = array(
			'name'                  => _x( 'Kind Words', 'Post Type General Name', 'grace-k' ),
			'singular_name'         => _x( 'Kind Word', 'Post Type Singular Name', 'grace-k' ),
			'menu_name'             => __( 'Kind Words', 'grace-k' ),
			'name_admin_bar'        => __( 'Kind Word', 'grace-k' ),
			'archives'              => __( 'Kind Word Archives', 'grace-k' ),
			'attributes'            => __( 'Kind Word Attributes', 'grace-k' ),
			'parent_item_colon'     => __( 'Parent Kind Word:', 'grace-k' ),
			'all_items'             => __( 'All Kind Words', 'grace-k' ),
			'add_new_item'          => __( 'Add New Kind Word', 'grace-k' ),
			'add_new'               => __( 'Add New', 'grace-k' ),
			'new_item'              => __( 'New Kind Word', 'grace-k' ),
			'edit_item'             => __( 'Edit Kind Word', 'grace-k' ),
			'update_item'           => __( 'Update Kind Word', 'grace-k' ),
			'view_item'             => __( 'View Kind Word', 'grace-k' ),
			'view_items'            => __( 'View Kind Words', 'grace-k' ),
			'search_items'          => __( 'Search Kind Word', 'grace-k' ),
			'not_found'             => __( 'Not found', 'grace-k' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'grace-k' ),
			'featured_image'        => __( 'Featured Image', 'grace-k' ),
			'set_featured_image'    => __( 'Set featured image', 'grace-k' ),
			'remove_featured_image' => __( 'Remove featured image', 'grace-k' ),
			'use_featured_image'    => __( 'Use as featured image', 'grace-k' ),
			'insert_into_item'      => __( 'Insert into kind word', 'grace-k' ),
			'uploaded_to_this_item' => __( 'Uploaded to this kind word', 'grace-k' ),
			'items_list'            => __( 'Kind Words list', 'grace-k' ),
			'items_list_navigation' => __( 'Kind Words list navigation', 'grace-k' ),
			'filter_items_list'     => __( 'Filter kind words list', 'grace-k' ),
		);
		$rewrite = [
			'slug' => 'kind-word',
		];
		$args    = array(
			'label'               => __( 'Kind Word', 'grace-k' ),
			'description'         => __( 'Kind Words Description', 'grace-k' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_rest'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => false,
			'capability_type'     => 'post',
			'rewrite'             => $rewrite,
		);

		register_post_type( 'kind-word', $args );
	}
}
